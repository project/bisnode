<?php

namespace Drupal\bisnode;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Class BisnodeService.
 */
class BisnodeService implements BisnodeServiceInterface {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The bisnode url.
   *
   * @var string
   */
  protected $url;

  /**
   * A base64 encode of credentials.
   *
   * @var string
   */
  protected $credentials;

  /**
   * Constructs a new BisnodeService object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle HTTP Client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;

    $config = \Drupal::config('bisnode.bisnodeconfig');
    $this->url = $config->get('bisnode_url');
    $this->credentials = base64_encode($config->get('bisnode_username') . ':' . $config->get('bisnode_password'));
  }

  /**
   * Search in Directory service.
   *
   * @param string $search
   *   Search parameter.
   *
   * @return array
   *   Return array of search result.
   */
  public function getDirectory($search): array {
    $uri = $this->url . '/search/norway/directory';

    $response = $this->post($uri, $search);
    $json = (string) $response->getBody();
    $result = json_decode($json, FALSE);

    if (isset($result->Result)) {
      return $this->buildValues($result->Result);
    }

    return [];
  }

  /**
   * Build values that will be used by webform.
   *
   * @param array $results
   *   Result array for bulding the webform.
   *
   * @return array
   *   Return webform.
   */
  protected function buildValues(array $results): array {
    if (empty($results)) {
      return [];
    }

    foreach ($results as $result) {
      $this->buildFullName($result);
      $this->buildFinalAddress($result, $include_city = FALSE);
      $this->buildDateBirth($result);
    }

    return $results;
  }

  /**
   * Build the date of birth.
   */
  protected function buildDateBirth(\stdClass $result) {
    if ($result->born) {
      $result->date_birth = date('Y-m-d', strtotime($result->born));
    }
    else {
      $result->date_birth = '';
    }
  }

  /**
   * Build the full name.
   */
  protected function buildFullName(\stdClass $result) {
    // Complete fullname.
    $fullname = [];
    if ($result->firstname) {
      $fullname[] = $result->firstname;
    }
    if ($result->middlename) {
      $fullname[] = $result->middlename;
    }
    if ($result->lastname) {
      $fullname[] = $result->lastname;
    }

    $result->fullname = implode(' ', $fullname);
  }

  /**
   * Build the final address.
   *
   * Get the last address looking for "firstaquired" and "lastaquired" keys.
   */
  protected function buildFinalAddress(\stdClass $result, bool $include_city = TRUE) {

    $full_address = [];
    if ($result->streetname) {
      $full_address[] = $result->streetname;
    }
    if ($result->houseno) {
      $full_address[] = $result->houseno;
    }
    if ($result->entrance) {
      $full_address[] = $result->entrance;
    }
    $result->final_address = implode(' ', $full_address);

    if ($include_city && $result->city) {
      $result->final_address .= ', ' . $result->city;
    }
  }

  /**
   * Request a post to the webservices.
   *
   * @param string $uri
   *   URL.
   * @param string $search
   *   Search string.
   * @param array $options
   *   Options array.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Return response object.
   */
  protected function post($uri, $search, array $options = []): ResponseInterface {
    $options += [
      'SearchMode'        => 0,
      'OnlyFoundWords'    => FALSE,
      'ListingType'       => 0,
      'ResultLimitSearch' => 10,
    ];

    return $this->httpClient->post(
      $uri,
      [
        'headers'            => [
          'Authorization' => ['Basic ' . $this->credentials],
        ],
        RequestOptions::JSON => [
          'Form'    => [
            'Type'         => 'Freetext',
            'Searchstring' => $search,
          ],
          'Options' => $options,
        ],
      ]
    );
  }

  /**
   * Get the fields to be mapping.
   */
  public static function fieldsMapping(): array {
    return [
      'fullname'      => t('Full name'),
      'firstname'     => t('First name'),
      'lastname'      => t('Last name'),
      'final_address' => t('Address'),
      'date_birth'    => t('Date of birth'),
      'zipcode'       => t('Zip code'),
      'city'          => t('City'),
    ];
  }

}
